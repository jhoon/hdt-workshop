package com.example;

import net.serenitybdd.rest.SerenityRest;

import org.junit.Assert;
import org.junit.Test;

import io.restassured.response.Response;

public class MainTest {
    @Test
    public void testGetUser() {
        Response response = SerenityRest.get("https://reqres.in/api/users/2");
        System.out.println(response.getBody().asString());
        Assert.assertEquals(200, response.getStatusCode());
    }
}
